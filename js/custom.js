var number = [10,20,30,40,50,60,70,80,90,100]


// find the element
number[6]
//output
// 70

number.length
//output
// 10

number.join(",")
//output
"10,20,30,40,50,60,70,80,90,100"

number.indexOf(40)
//output
// 3

// adding new element
number.push("test")
// output
11 //(number length)

// change the value
number[3] = 1
//output
//number
//(11) [10, 20, 30, 1, 50, 60, 70, 80, 90, 100, "test"]

// Remove the last element
number.pop()
//output
// "test"

// Remove the first element
number.shift()
//output
// 10

// Remove mention the number element
number.slice(2,5)
//output
// (3) [30, 40, 50]


var i, ele;

for(i=0; i<5; i++) {
  ele = document.createElement('div');
  ele.textContent = i;
  document.body.appendChild(ele);
}

for (var i = 0; i < 5; i++) {
  console.log('names' + i);
}

var i = 0;
while (i < 10) {
  i++;
  if (i === 5) continue;
  console.log(i);
}


//Objects

student = {
  "name" : "john",
  "age" : 28,
  "mark" : 90
}

student.name
//output
//"john"

student.age
//output
//28

//Array inside
students = [
  {
    "name" : "Dany",
    "age" : 30,
    "mark" : 95
  },
  {
    "name" : "Ann",
    "age" : 26,
    "mark" : 90
  }
]

students[0]
//output
//{name: "Dany", age: 30, mark: 95}

students[1]
//output
//{name: "Ann", age: 26, mark: 90}

students[0].age
//output
// 30

//functions

let sayHai = function () {
  console.log("Greting message for user");
  console.log("Hey User");
}

sayHai();

let sayHello = function (name) {
  console.log("Greting message for user");
  console.log(`Hey ${name}`);
}

sayHello('John');

var fullNameMaker = function (firstname, lastname) {
  console.log('Welcome');
  console.log(`Hai ${firstname} ${lastname}`);
}

fullNameMaker('John', 'Doe');

function greet(name) {
  console.log('Hello ' + name);
}

greet('John');
greet('Mary');

function greetOne(name, lastName) {
  console.log('Hello ' + name + ' ' + lastName);
}

greetOne('John', 'Smith');

var drink_tea = 20;
var eat_burger = 150;
var eact_pizza = 300;
var drink_water = 50;

var mealList = [drink_tea, eat_burger, eact_pizza, drink_water];
function visitCarnival(mList){
  for(var i=0, num=mList.length; i<num; i++){
    console.log('mealList');
  }
}

visitCarnival(mealList); 


